package com.demo.springboot.dto;

public class ParamsDto {
    double a,b,c;

    public ParamsDto(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public ParamsDto() {
    }
}
