package com.demo.springboot.dto;

public class ResultDto {

    Double x1,x2;
    String direction;
    Vertex W;

    public ResultDto(Double x1, Double x2, String direction, Vertex w) {
        this.x1 = x1;
        this.x2 = x2;
        this.direction = direction;
        W = w;
    }

    public Vertex getW() {
        return W;
    }

    public String getDirection() {
        return direction;
    }

    public ResultDto() {
    }

    public Double getX1() {
        return x1;
    }

    public Double getX2() {
        return x2;
    }
}
