package com.demo.springboot.dto;

public class Vertex {

    double p,q;

    public Vertex() {
    }

    public Vertex(double p, double q) {
        this.p = p;
        this.q = q;
    }

    public double getP() {
        return p;
    }

    public double getQ() {
        return q;
    }
}
