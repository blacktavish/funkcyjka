package com.demo.springboot.rest;


import com.demo.springboot.dto.AlertDto;
import com.demo.springboot.dto.ParamsDto;
import com.demo.springboot.dto.ResultDto;
import com.demo.springboot.service.Quadratic;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

@RestController
public class DocumentApiController {

    private final Quadratic quadratic;

    //@Autowired
    public DocumentApiController(Quadratic quadratic) {
        this.quadratic = quadratic;
    }

    @ExceptionHandler
    public ResponseEntity<AlertDto> missing(MissingServletRequestParameterException ex){
        String name = ex.getParameterName();
        return new ResponseEntity<>(new AlertDto("Missing parameter: " + name), HttpStatus.BAD_REQUEST);
    }
    @GetMapping("api/math/quadratic/{a}/{b}/{c}")
    public ResponseEntity<ResultDto> calculate(@PathVariable double a, @PathVariable double b, @PathVariable double c){
        return new ResponseEntity<>(quadratic.Calculate(a,b,c), HttpStatus.OK);
    }

    @GetMapping("api/math/quadratic-function")
    public ResponseEntity<ResultDto> calculateGet(@RequestParam double a, @RequestParam double b , @RequestParam double c){
        return new ResponseEntity<>(quadratic.Calculate(a,b,c), HttpStatus.OK);
    }
    @PostMapping("api/math/quadratic-function")
    public ResponseEntity<ResultDto> calculateFunction(@RequestBody ParamsDto paramsDto){
        return new ResponseEntity<>(quadratic.Calculate(paramsDto.getA(),paramsDto.getB(), paramsDto.getC()), HttpStatus.OK);
    }


    //private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);

}
