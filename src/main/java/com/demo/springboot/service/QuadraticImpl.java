package com.demo.springboot.service;

import com.demo.springboot.dto.ResultDto;
import com.demo.springboot.dto.Vertex;
import org.springframework.stereotype.Service;

@Service
public class QuadraticImpl implements Quadratic{

    @Override
    public ResultDto Calculate(double a, double b, double c) {
        Double x1,x2;
        double delta;
        double p,q;
        String direction = "";
        delta = (b*b) - 4 * a * c;

        if(a > 0){
            direction = "Parabole skierowane w górę";
        }else if(a < 0){
            direction = "Parabole skierowane w dół";
        }


        if(delta > 0){
            x1 = (-b-Math.sqrt(delta))/(2*a);
            x2 = (-b+Math.sqrt(delta))/(2*a);
        }else if(delta == 0){
            x1 = -b/(2*a);
            x2 = null;
        }
        else{
            x1 = null;
            x2 = null;
        }

        p = -b/(2*a);
        q = -delta/(4*a);



        return new ResultDto(x1,x2, direction, new Vertex(p,q));
    }
}
