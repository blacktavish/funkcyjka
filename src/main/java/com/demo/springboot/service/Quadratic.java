package com.demo.springboot.service;

import com.demo.springboot.dto.ResultDto;

public interface Quadratic {
    ResultDto Calculate(double a, double b, double c);
}
